from app import umm
import unittest

class TestUmm(unittest.TestCase):
  def test_add(self):
    endpoint = "/add?a=1&b=2"
    response = test_client.get(endpoint)
    result = int(response.get_data(as_text=True))
    self.assertEqual(result, 3)
    print("- " + endpoint + "\n" + str(result) + "\n")

    endpoint = "/add?a=3&b=5"
    response = test_client.get(endpoint)
    result = int(response.get_data(as_text=True))
    self.assertEqual(result, 8)
    print("- " + endpoint + "\n" + str(result) + "\n")

  def test_sub(self):
    endpoint = "/sub?a=3&b=5"
    response = test_client.get(endpoint)
    result = int(response.get_data(as_text=True))
    self.assertEqual(result, -2)
    print("- " + endpoint + "\n" + str(result) + "\n")

if __name__ == "__main__":
    test_client = umm.test_client()
    unittest.main()

