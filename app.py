from flask import Flask, request

umm = Flask(__name__)

@umm.route('/', methods=['GET'])
def index():
    return 'Hello Bob from wine32on64'

@umm.route("/add", methods=['GET'])
def add():
    param_a = request.args.get('a', default = None, type = int)
    param_b = request.args.get('b', default = None, type = int)

    if param_a is not None and param_b is not None:
        return str(param_a + param_b), 200
    else:
        return "Bad Request", 400

@umm.route("/sub", methods=['GET'])
def sub():
    param_a = request.args.get('a', default = None, type = int)
    param_b = request.args.get('b', default = None, type = int)

    if param_a is not None and param_b is not None:
        return str(param_a - param_b), 200
    else:
        return "Bad Request", 400

if __name__ == "__main__":
    umm.run(host='0.0.0.0', port=8194)

