# umm


## Name
the 'umm' assignment repository.

## Description
This repository is the BoB development CI test exercise project. <br>
app.py was implemented as a very simple `add`, `sub` endpoints. <br>
test_app.py was implemented as a test suite using a python unittest module with an internal werkzeug test client, not a python requests module. <br>

A CI stage will do the testing
```
 /add?a=1&b=2 -> must be  '3'

 /add?a=3&b=5 -> must be  '8'

 /sub?a=3&b=5 -> must be '-2'
```
this above.

## Authors
- [hurrhnn](https://github.com/hurrhnn)

## License
This project is licensed under the Unlicense License - see the [LICENSE](https://gitlab.com/hurrhnn/umm/-/blob/main/LICENSE) file for details.

